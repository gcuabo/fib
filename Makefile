run:
	docker-compose -f compose/dev/app.yml up --build
celery_up:
	docker-compose -f compose/dev/celery.yml up --build
base_up:
	docker-compose -f compose/dev/db.yml -f compose/dev/redis.yml up -d

## TESTING
test_base:
	docker-compose -f compose/test/test.yml up db_testing redis_testing
test:
	docker-compose -f compose/test/test.yml up --build zapp_testing

### Migrate
migrate:
	docker exec -ti app_dev sh -c "flask db upgrade"

### Kill all containers
kill_all:
	docker ps -q | xargs docker kill
