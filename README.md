# Fib



## Getting started

Run base services
    - redis
    - postgres

```shell
make base_up
```

Boot up celery service

```shell
make celery_up
```

Boot up application

```shell
make run
```

Wait for `make run` to finish, then run migrations:

```shell
make migrate
```

Try to post to test ping endpoint
```shell
curl http://localhost:5000/ping
```

Test the post endpoint
```shell
curl -d '{"n": 10}' -H "Content-Type: application/json" -X POST http://localhost:5000/fibonacci
```

Output should look like:
```shell
{"status": "pending", "nth": "None"}
```

Kill all containers
```shell
make kill_all
```

## Testing

To run, boot up base test service:

```shell
make test_base
```

And then run the tests

```shell
make test
```

## Tech debts (among the obvs)

* Schema still needs tests
* Dump schema to some sql and load db from there instead
