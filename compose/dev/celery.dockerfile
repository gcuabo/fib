FROM python:3.9.5-alpine

RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev

COPY . /app
WORKDIR /app
RUN pip install pipenv
RUN pipenv install --system --deploy --dev

ENV FLASK_APP=core.application:create_flask_app
