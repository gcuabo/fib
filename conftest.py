import os

import pytest
from faker import Faker

from tests.fixtures import *


@pytest.fixture(scope="session")
def app(request):
    os.environ["FLASK_ENV"] = "testing"
    from core.application import create_flask_app

    app = create_flask_app()
    ctx = app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()

    return app


@pytest.fixture
def faker():
    return Faker()
