from flask import Response, request
from flask_restful import Resource

from core.models.fibonacci import Fibonacci
from core.schema.fibonacci import (FibonacciCreateSchema,
                                   FibonacciResponseSchema)
from core.tasks import calculate_fib_worker


class FiboacciResource(Resource):
    def post(self):
        payload = FibonacciCreateSchema().load(request.json)
        n = payload.get("n")

        # check if exists
        exists = Fibonacci.get_by_id(id=n)
        return_value, status = None, "pending"

        if exists:
            return_value = exists.fib_value
            status = "success"
        else:
            calculate_fib_worker.delay(n)

        response = {"nth": str(return_value), "status": status}

        return Response(
            FibonacciResponseSchema().dumps(response),
            status=201,
            mimetype="application/json",
        )
