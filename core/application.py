import os

from celery import Celery
from flask import Flask
from flask_migrate import Migrate
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

from .celery import init_celery
from .config import Config
from .urls import register_api_urls

db = SQLAlchemy()
migrate = Migrate()

celery_broker = os.getenv("CELERY_BROKER") or "redis://localhost:6379"
celery_result_backend = os.getenv("CELERY_BACKEND") or "redis://localhost:6379"

celery = Celery(
    __name__,
    broker=celery_broker,
    result_backend=celery_result_backend,
    include=["core.tasks"],
)


def create_flask_app():
    app = Flask(__name__)

    # Get the run time env
    run_time_env = os.environ.get("FLASK_ENV", "development")

    # Configure from config file
    app.config.from_object(Config(run_time_env))

    # sqlachemy
    db.init_app(app)
    migrate.init_app(app, db)

    # add context to task
    init_celery(app, celery)

    # make flask restfuls
    api = Api(app)
    register_api_urls(api)

    return app
