from sqlalchemy import Column, Integer

from core.application import db


class Fibonacci(db.Model):
    __tablename__ = "fibonacci"

    id = Column(Integer, primary_key=True)
    fib_value = Column(Integer, nullable=False)

    @classmethod
    def get_by_id(cls, id: int) -> "Fibonacci":
        return cls.query.filter_by(id=id).one_or_none()
