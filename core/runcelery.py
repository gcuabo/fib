from core.application import celery, create_flask_app
from core.celery import init_celery

app = create_flask_app()
init_celery(app, celery)
