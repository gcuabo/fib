from marshmallow import Schema, fields


class FibonacciCreateSchema(Schema):
    n = fields.Integer(required=True)


class FibonacciResponseSchema(Schema):
    nth = fields.Str(required=True)
    status = fields.Str(required=True)
