from core.application import celery, db
from core.models.fibonacci import Fibonacci
from core.utils import create_fibonacci


@celery.task
def calculate_fib_worker(n: int):
    fib_value = create_fibonacci(n)
    fib = Fibonacci(id=n, fib_value=fib_value)
    db.session.add(fib)
    db.session.commit()
