def register_api_urls(api):
    from .api.fibonacci import FiboacciResource
    from .api.ping import Ping

    resources = [(Ping, "/ping"), (FiboacciResource, "/fibonacci")]

    for resource_url in resources:
        api.add_resource(*resource_url)
