__all__ = ("create_fibonacci",)


def create_fibonacci(num: int) -> int:
    """
    TODO:
    This can be memoized from db entries

    :param num:
    :return:
    """
    assert isinstance(num, int)

    if num in {0, 1}:  # Base case
        return num
    return create_fibonacci(num - 1) + create_fibonacci(num - 2)
