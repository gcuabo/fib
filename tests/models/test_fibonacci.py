from core.application import db
from core.models.fibonacci import Fibonacci


def test_get_by_id():
    assert callable(Fibonacci.get_by_id)


def test_get_by_id_success(app, faker):
    get_by_id = Fibonacci.get_by_id
    random_id = faker.unique.pyint()

    assert get_by_id(random_id) is None

    fib_obj = Fibonacci(id=random_id, fib_value=faker.pyint())
    db.session.add(fib_obj)
    db.session.commit()

    fib_obj_from_method = get_by_id(random_id)
    assert fib_obj_from_method is not None
    assert isinstance(fib_obj_from_method, Fibonacci)
