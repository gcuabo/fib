import json
import random

from faker import Faker

from core.application import db
from core.models.fibonacci import Fibonacci
from core.utils import create_fibonacci

fake = Faker()


def test_fibonacci(client):
    # Post once, n doesnt exist yet
    payload = {"n": random.randint(0, 20)}
    response = client.post(
        "/fibonacci",
        data=json.dumps(payload),
        headers={"content-type": "application/json"},
    )
    assert response.status_code == 201

    json_response = response.json
    assert "nth" in json_response
    assert "status" in json_response

    assert json_response["nth"] == "None"
    assert json_response["status"] == "pending"


def test_fib_value_already_exists(client):
    n = random.randint(0, 20)
    fib_value = create_fibonacci(n)
    payload = {"n": n}

    fib = Fibonacci(id=n, fib_value=fib_value)
    db.session.add(fib)
    db.session.commit()

    # n now exists, nth shoud not be None
    response = client.post(
        "/fibonacci",
        data=json.dumps(payload),
        headers={"content-type": "application/json"},
    )
    assert response.status_code == 201

    json_response = response.json
    assert "nth" in json_response
    assert "status" in json_response

    assert isinstance(json_response["nth"], str)
    assert json_response["status"] == "success"
    assert json_response["nth"] == str(fib_value)
