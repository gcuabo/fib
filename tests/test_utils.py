import pytest

from core.utils import create_fibonacci


def test_create_fibonacci():
    first_15_values = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377]

    for index, value in enumerate(first_15_values):
        result = create_fibonacci(index)
        assert isinstance(result, int)
        assert result == first_15_values[index]


def test_create_fibonacci_defensive_asserts():
    invalid_method_arg = "not_a_number"
    with pytest.raises(AssertionError):
        create_fibonacci(invalid_method_arg)
